package enlighten;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Alerting {

	int satelliteId;
	String severity;
	String highComponent_a;
	String highComponent_b;
	String lowComponent_a;
	String lowComponent_b;
	String lowTimestamp_a;
	String lowTimestamp_b;
	String highTimestamp_a;
	String highTimestamp_b;

	public void printTelemetryAlerts(String fileName) throws IOException {
		List<String> result;
		try (Stream<String> lines = Files.lines(Paths.get(fileName))) {
			result = lines.collect(Collectors.toList());
		}

		List<String> Satilite_a = new ArrayList<String>();
		List<String> Satilile_b = new ArrayList<String>();

		for (int i = 0; i < result.size(); i++) {
			String printString = result.get(i);
			if (printString.contains("1000")) {
				Satilite_a.add(printString);
			}
		}
		for (int i = 0; i < result.size(); i++) {
			String printString = result.get(i);
			if (printString.contains("1001")) {
				Satilile_b.add(printString);
			}
		}

		int redHighErrorCount_a = 0;
		int redLowErrorCount_a = 0;
		double redHigh_a = 0;
		double redLow_a = 0;
		double rawValue_a = 0;
		String lastHighTimeStamp_aa = null;
		String lastHighComponent_aa = null;
		String lastLowTimeStamp_aa = null;
		String lastLowComponent_aa = null;
		for (int i = 0; i < Satilite_a.size(); i++) {

			String aSatellite = Satilite_a.get(i);

			String[] aSatelliteSplit = aSatellite.split("\\|");

			redHigh_a = Double.parseDouble(aSatelliteSplit[2]);
			redLow_a = Double.parseDouble(aSatelliteSplit[5]);
			rawValue_a = Double.parseDouble(aSatelliteSplit[6]);

			if (rawValue_a > redHigh_a) {
				redHighErrorCount_a++;
				lastHighTimeStamp_aa = aSatelliteSplit[0];
				lastHighComponent_aa = aSatelliteSplit[7];
			}
			if (rawValue_a < redLow_a) {
				redLowErrorCount_a++;
				lastLowTimeStamp_aa = aSatelliteSplit[0];
				lastLowComponent_aa = aSatelliteSplit[7];
			}
		}

		highTimestamp_a = lastHighTimeStamp_aa;
		lowTimestamp_a = lastLowTimeStamp_aa;
		highComponent_a = lastHighComponent_aa;
		lowComponent_a = lastLowComponent_aa;

		int redHighErrorCount_b = 0;
		int redLowErrorCount_b = 0;
		String timeStamp_b;
		double redHigh_b;
		double redLow_b;
		double rawValue_b;
		String component_b = null;
		String lastHighTimeStamp_bb = null;
		String lastHighComponent_bb = null;
		String lastLowTimeStamp_bb = null;
		String lastLowComponent_bb = null;
		for (int t = 0; t < Satilile_b.size(); t++) {

			String bSatellite = Satilile_b.get(t);

			String[] bSatelliteSplit = bSatellite.split("\\|");

			timeStamp_b = bSatelliteSplit[0];
			redHigh_b = Double.parseDouble(bSatelliteSplit[2]);
			redLow_b = Double.parseDouble(bSatelliteSplit[5]);
			rawValue_b = Double.parseDouble(bSatelliteSplit[6]);
			component_b = bSatelliteSplit[7];

			if (rawValue_b > redHigh_b) {
				redHighErrorCount_b++;
				lastHighTimeStamp_bb = bSatelliteSplit[0];
				lastHighComponent_bb = bSatelliteSplit[7];
			}
			if (rawValue_b < redLow_b) {
				redLowErrorCount_b++;
				lastLowTimeStamp_bb = bSatelliteSplit[0];
				lastLowComponent_bb = bSatelliteSplit[7];
			}
		}

		highTimestamp_b = lastHighTimeStamp_bb;
		lowTimestamp_b = lastLowTimeStamp_bb;
		highComponent_b = lastHighComponent_bb;
		lowComponent_b = lastLowComponent_bb;

		if (redHighErrorCount_a >= 3) {
			satelliteId = 1000;
			String severity = "RED HIGH";
			String component = this.highComponent_a;
			String timeStamp = this.highTimestamp_a;

			Gson gson = new GsonBuilder().setPrettyPrinting().create();

			String[] redHighErrorPrint = { "satelliteId: " + satelliteId, "severity: " + severity,
					"component: " + component, "timestamp: " + timeStamp };

			String jsonPrint = gson.toJson(redHighErrorPrint);

			System.out.println(jsonPrint + ",");

		}

		if (redLowErrorCount_a >= 3) {
			satelliteId = 1000;
			String severity = "RED LOW";
			String component = this.lowComponent_a;
			String timestamp = this.lowTimestamp_a;

			Gson gson = new GsonBuilder().setPrettyPrinting().create();

			String[] redHighErrorPrint = { "satelliteId: " + satelliteId, "severity: " + severity,
					"component: " + component, "timestamp: " + timestamp };

			String jsonPrint = gson.toJson(redHighErrorPrint);

			System.out.println(jsonPrint);

		}

		if (redHighErrorCount_b >= 3) {
			satelliteId = 1001;
			severity = "RED HIGH";
			String component = this.highComponent_b;
			String timestamp = this.highTimestamp_b;

			Gson gson = new GsonBuilder().setPrettyPrinting().create();

			String[] redHighErrorPrint = { "satelliteId: " + satelliteId, "severity: " + severity,
					"component: " + component, "timestamp: " + timestamp };

			String jsonPrint = gson.toJson(redHighErrorPrint);

			System.out.println(jsonPrint + ",");
		}

		if (redLowErrorCount_b >= 3) {
			satelliteId = 1001;
			severity = "RED LOW";
			component_b = this.lowComponent_b;
			timeStamp_b = this.lowTimestamp_b;

			Gson gson = new GsonBuilder().setPrettyPrinting().create();

			String[] redHighErrorPrint = { "satelliteId: " + satelliteId, "severity: " + this.severity,
					"component: " + component_b, "timestamp: " + timeStamp_b };

			String jsonPrint = gson.toJson(redHighErrorPrint);

			System.out.println(jsonPrint);
		}

	}

	public static void main(String[] args) throws IOException {

		String localFileName = "C:\\Users\\benec\\eclipse-workspace\\BeneckEnlighten\\input\\telemetry.txt";

		Alerting alerting = new Alerting();

		alerting.printTelemetryAlerts(localFileName);

	}

}
